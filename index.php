<?php

// Simple API fetching all users to be displayed on a specific page for a specific route with admin notification option

// DATABASE CONNECTION
require_once 'includes/init.php';

// PHPMAILER INCLUDES
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

// New instance of PHPMailer
$mail = new PHPMailer(true);

// POSTED VARIABLES 
// Stored in $p object
$p = new StdClass();
$p->method = $_POST['method'];
$p->route_id = $_POST['routeId'];
$p->date = $_POST['date'];
$p->page = $_POST['page'];
$p->user_id = $_POST['userId'];
$p->missing_hap = json_decode($_POST['hap'], true);

// $p->method = 'getUsers';
// $p->route_id = '136';
// $p->date = '20180822';
// $p->page = '1';

// RESPONSE VARIABLE
$response = new StdClass();
$response->status = 'fail';

// ADMINISTATORS TO NOTIFY
$admin_emails = ['2mc@tswarteschaap.nl'];

if (isset($p->method)) {

  function get_users(){
    global $db, $p;

    $route_id = $p->route_id;
    $date = $p->date;
    $page = $p->page;

    // Collection array
    $hap_in_route = [];

    // GET USERS FROM USER_ROUTES
    $user_routes_query = $db->query("SELECT user_id FROM user_routes WHERE route_id = '$route_id' ORDER BY route_place ASC");
    $users = $user_routes_query->fetchAll(PDO::FETCH_ASSOC);

    // FETCH USERS FROM 'users' TABLE
    foreach ($users as $id) {
      extract($id);
      $user_query = $db->query("SELECT standaardWaarde, identity FROM users WHERE user_id = '$user_id'");
      // Check if there's a result
      $result_count = $user_query->rowCount();
      if ($result_count > 0) {
      
        $user_result = $user_query->fetchAll(PDO::FETCH_ASSOC);

        $hap_info = new StdClass();
        $hap_info->user_id = $user_id;
        $hap_info->identity = $user_result[0]['identity'];

        // Extract 'standaardwaarde'
        $standard_value = $user_result[0]['standaardWaarde'];

        if ($standard_value === 'nee') {
          $aanwezig_query = $db->query("SELECT afwezig FROM aanwezig WHERE user_id = '$user_id' AND datum = '$date'");
          $aanwezig = $aanwezig_query->fetchAll(PDO::FETCH_ASSOC);

          // Check whether combination is found in table
          if(array_key_exists(0, $aanwezig)){
            $value = $aanwezig[0]['afwezig'];

            // If value equals 0, the HAP is added to the list
            if ($value === '0') {
              array_push($hap_in_route, $hap_info); 
            }

          }

        }else{
          $aanwezig_query = $db->query("SELECT afwezig FROM aanwezig WHERE user_id = '$user_id' AND datum = '$date'");
          $aanwezig = $aanwezig_query->fetchAll(PDO::FETCH_ASSOC);

          // Check whether combination is found in table
          if(array_key_exists(0, $aanwezig)){
            $value = $aanwezig[0]['afwezig'];

            // If value equals 0, the HAP is added to the list
            if ($value === '0') {
              array_push($hap_in_route, $hap_info); 
            }

          }else{
            // If there's no entry, HAP is added to the list
            array_push($hap_in_route, $hap_info); 
          }

        }

      }    
      

    }

    // Each page contains 10 items
    // 2nd page would contain indexes 10 through 19

    $index_start = ($page-1)*10;
    $amount_to_slice = 10;

    $hap_on_page = array_slice($hap_in_route, $index_start, $amount_to_slice);

    $response->status = 'success';
    $response->results = $hap_on_page;

    echo json_encode($response);

  }
  
  function notify_admins(){
    global $db, $p, $response, $admin_emails, $mail;

    // Get name of route
    $route_name_query = $db->query("SELECT name FROM routes WHERE route_id = '$p->route_id'");
    $route_name_results = $route_name_query->fetchAll(PDO::FETCH_ASSOC);
    $route_name = $route_name_results[0]['name'];

    // Get name of user
    $user_name_query = $db->query("SELECT identity FROM users WHERE user_id = '$p->user_id'");
    $user_name_results = $user_name_query->fetchAll(PDO::FETCH_ASSOC);
    $user_name = $user_name_results[0]['identity'];

    $title = 'Missende praktijken op routepagina geconstateerd';
    $message = 'Er zijn missende praktijken op een routepagina geconstateerd.
                <br/><b>Route:</b> ' . $route_name . '
                <br/><b>User:</b> ' . $user_name . '
                <br /><br />
                <b>Praktijk(en):</b><br />';

    foreach ($p->missing_hap as $hap_id) {

      // Get name of HAP
      $hap_name_query = $db->query("SELECT identity FROM users WHERE user_id = '$hap_id'");
      $hap_name_results = $hap_name_query->fetchAll(PDO::FETCH_ASSOC);
      $hap_name = $hap_name_results[0]['identity'];

      $message .= $hap_name . '<br />';
    }

    try {
      //Server settings
      $mail->isSMTP();                                        // Set mailer to use SMTP
      $mail->Host         = 'mail.antagonist.nl';             // Specify main and backup SMTP servers
      $mail->SMTPAuth   	= true;                             // Enable SMTP authentication
      $mail->Username     = "noreply@bvobaarmoederhals.nl";   // SMTP username
      $mail->Password     = "0Myjmk";                         // SMTP password
      $mail->SMTPSecure   = 'tls';                            // Enable TLS encryption, `tls` also accepted
      $mail->Port         = 587;                              // TCP port to connect to
  
      //Recipients
      $mail->ClearAllRecipients();
      $mail->setFrom('info@medicalogistics.nl', '2MC Routes');
      
      // Message is sent to all defined admins
      foreach ($admin_emails as $email) {
        $mail->addAddress($email);
      }

      $mail->addReplyTo('info@medicalogistics.nl', 'Information');
  
      $mail->Subject = $title;
      
      $mail->Body = '<!DOCTYPE html>
                  <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
                  <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
                  <!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
                  <!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
                  <head>
                  <meta charset="utf-8">
                  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  
                  <title>2MC mailer</title>
                  <meta name="description" content="2MC Mailer">
                  <meta name="keywords" content="">
  
                  <!-- Mobile viewport -->
                  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
  
                  <link rel="shortcut icon" href="images/favicon.ico"  type="image/x-icon">
  
                  <link rel="stylesheet" href="http://www.bvobaarmoederhals.nl/css/normalize.css">
                  <link rel="stylesheet" href="http://www.bvobaarmoederhals.nl/js/flexslider/flexslider.css">
                  <link rel="stylesheet" href="http://www.bvobaarmoederhals.nl/css/basic-style.css">
  
                  <!-- end CSS-->
                      
                  <!-- JS-->
                  <script src="js/libs/modernizr-2.6.2.min.js"></script>
                  <!-- end JS-->
                  </style>
                  </head>
  
                  <body id="home">
                    
                  <!-- header area -->
                  <header class="wrapper clearfix">
                          
                    <div id="banner">        
                      <div id="logo"><img src="https://medicalogistics.nl/img/logo.png" alt="logo"></div> 
                    </div>
                    
                  </header><!-- end header -->
                  
                  <!-- main content area -->   
                  <div id="main" class="wrapper">
                      
                  <!-- content area -->    
                    <section id="content" class="wide-content">
                        <div class="row">	
                          
                              <p>'.$message.'</p>
                          
                      </div><!-- end row -->
                    </section><!-- end content area -->   
                  <!-- footer area -->    
                  <footer>
                    <div id="colophon" class="wrapper clearfix">
                        Contactgegevens: Commpanionz - Zomerkade 102 - 1273 SP - Huizen - The Netherlands - Telefoon: +31 341 495 254 - Website: www.commpanionz.org - Mail: <a href="mailto:info@bvobaarmoederhals.nl" style="color:#fff">info@bvobaarmoederhals.nl</a>
                      </div>
                  </footer><!-- #end footer area --> 
                  <!-- jQuery -->
                  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
                  <script>window.jQuery || document.write("<script src="js/libs/jquery-1.9.0.min.js">\x3C/script>")</script>
  
                  <script defer src="js/flexslider/jquery.flexslider-min.js"></script>
  
                  <!-- fire ups - read this file!  -->   
                  <script src="js/main.js"></script>
  
                  </body>
                  </html>';
      $mail->isHTML(true);       // Set email format to HTML
      $mail->AltBody = $title;
      $mail->send();
      $response->status = 'success';
      $response->message = 'Message successfully sent to admins';
    } 
      catch (Exception $e) {
        $response->message = 'Mail not sent';
    }

    echo json_encode($response);
    
  }
  
  // SWITCH CHOOSING METHOD
  switch ($p->method) {
    case 'getUsers':
      get_users();
      break;

    case 'notify':
      notify_admins();
      break;
    
    default:
      $response->message = 'Not a valid method';
      echo json_encode($response);
      break;
  }

}else{
  // Wanneer method niet is meegegeven in POST
  $response->message = 'Method missing';
  echo json_encode($response);
}



